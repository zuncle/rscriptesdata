# RScriptesData library

## Install library in Rstudio

    > install.packages("devtools")
    > library("devtools")
    > install_bitbucket(repo="zuncle/rScriptesData")
    
## Setup and test RScriptesData
    
Check the IP Address of the Scriptes-datastore host computer and set the variable "url" to the right value:

    > url <- "http://<ip address or host name>" (e.g "http://168.192.1.10")

Check the port of the Scriptes-datastore host computer and set the variable "port" to the right value:
    
    > port <- "9000"

Find tour authentification token in the Scriptes-datastore website and set the variable "token" to the right value:
    
    > token <- "your token"

Execute the following function to get the list of ponctual sites:    
    
    > getSitePoints(url, port, token)
