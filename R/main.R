# Package RScriptesData
# You can learn more about package authoring with RStudio at:
#
#   http://r-pkgs.had.co.nz/
#
# Some useful keyboard shortcuts for package authoring:
#
#   Build and Reload Package:  'Cmd + Shift + B'
#   Check Package:             'Cmd + Shift + E'
#   Test Package:              'Cmd + Shift + T'

# install.packages("jsonlite", repos='http://cran.r-project.org')

# library("jsonlite")

#
# Examples :
# - getSitePoints("http://localhost", 9000, "8f17ef3c-0890-4d7f-adda-21fac57494fa")
# - getSitePoints("http://localhost", 9000, "8f17ef3c-0890-4d7f-adda-21fac57494fa", "EPSG:2154")
#
getSitePoints <- function(
  url,
  port,
  token,
  srs = "EPSG:4326"){

  if ( missing(url))
    stop("url is required (example 'http://192/168.1.23').")

  if ( missing(port))
    stop("port is required (example 9000).")

  if ( missing(token))
    stop("Authentification token is required.")

  route <- paste(url, ":", port,
                 "/rest/sitePoints?token=", token,
                 "&srs=", srs,
                 sep="")
  result <- jsonlite::fromJSON(txt=route)
  return (result)
}

#
# Examples :
# - getSeries("http://localhost", 9000, "8f17ef3c-0890-4d7f-adda-21fac57494fa")
#
getSeries <- function(
  url,
  port,
  token){

  if ( missing(url))
    stop("url is required (example 'http://192/168.1.23').")

  if ( missing(port))
    stop("port is required (example 9000).")

  if ( missing(token))
    stop("Authentification token is required.")

  route <- paste(url, ":", port,
                "/rest/seriesChronologiques?token=", token,
                sep="")
  result <- jsonlite::fromJSON(txt=route)
  return (result)
}

#
# Examples :
# - getNumberOfMeasurement("http://localhost", 9000, "8f17ef3c-0890-4d7f-adda-21fac57494fa","G9103020", "QJM", "HYDRO-QJM")
# - getNumberOfMeasurement("http://localhost", 9000, "8f17ef3c-0890-4d7f-adda-21fac57494fa","G9103020", "QJM", "HYDRO-QJM", strptime('2015-1-1T00:00:00',format='%FT%H:%M:%S'), strptime('2015-1-31T00:00:00',format='%FT%H:%M:%S'))
#
getNumberOfMeasurement <- function(
  url,
  port,
  token,
  codeSite,
  codeVariable,
  codeDispositifDeCollecte,
  beginDate = strptime('1900-1-31T00:00:00',format='%FT%H:%M:%S'),
  endDate = strptime('2100-12-31T23:59:59',format='%FT%H:%M:%S')){

  if ( missing(url))
    stop("url is required (example 'http://192/168.1.23').")

  if ( missing(port))
    stop("port is required (example 9000).")

  if ( missing(token))
    stop("Authentification token is required.")

  if ( missing(codeSite))
    stop("siteCode is required.")

  if ( missing(codeVariable))
    stop("codeVariable is required.")

  if ( missing(codeDispositifDeCollecte))
    stop("codeDispositifDeCollecte is required.")

  dateD <- format(beginDate,format="%FT%H:%M:%S")
  dateF <- format(endDate,format="%FT%H:%M:%S")

  route <- paste(url, ":", port,
                 "/rest/nombreMesures?token=", token,
                 "&codeSite=", codeSite,
                 "&codeVariable=", codeVariable,
                 "&codeDispositifDeCollecte=", codeDispositifDeCollecte,
                 "&dateDebut=", dateD,
                 "&dateFin=", dateF,
                 sep="")
  result <- jsonlite::fromJSON(txt=route)
  return (result)
}

#
# Example :
# - getMeasurements("http://localhost", 9000, "8f17ef3c-0890-4d7f-adda-21fac57494fa","G9103020", "QJM", "HYDRO-QJM")
# - getMeasurements("http://localhost", 9000, "8f17ef3c-0890-4d7f-adda-21fac57494fa", "G9103020", "QJM", "HYDRO-QJM", strptime('2015-1-1T00:00:00',format='%FT%H:%M:%S'), strptime('2015-1-31T00:00:00',format='%FT%H:%M:%S'))
# - getMeasurements("http://localhost", 9000, "8f17ef3c-0890-4d7f-adda-21fac57494fa", "G9103020", "QJM", "HYDRO-QJM", endDate = strptime("2015-01-31T23:59:59",format="%FT%H:%M:%S"), maxNumberOfMeasurement=31)
#
getMeasurements <- function(
  url,
  port,
  token,
  codeSite,
  codeVariable,
  codeDispositifDeCollecte,
  beginDate = strptime('1900-1-31T00:00:00',format='%FT%H:%M:%S'),
  endDate = strptime('2100-12-31T23:59:59',format='%FT%H:%M:%S'),
  maxNumberOfMeasurement = -1){

  if ( missing(url))
    stop("url is required (example 'http://192/168.1.23').")

  if ( missing(port))
    stop("port is required (example 9000).")

  if ( missing(token))
    stop("Authentification token is required.")

  if ( missing(codeSite))
    stop("siteCode is required.")

  if ( missing(codeVariable))
    stop("codeVariable is required.")

  if ( missing(codeDispositifDeCollecte))
    stop("codeDispositifDeCollecte is required.")

  dateD <- format(beginDate,format="%FT%H:%M:%S")
  dateF <- format(endDate,format="%FT%H:%M:%S")

  if (missing(maxNumberOfMeasurement)){
    route <- paste(url, ":", port,
                   "/rest/mesures?token=", token,
                   "&codeSite=", codeSite,
                   "&codeVariable=", codeVariable,
                   "&codeDispositifDeCollecte=", codeDispositifDeCollecte,
                   "&dateDebut=", dateD,
                   "&dateFin=", dateF,
                   sep="")
  }else{
    route <- paste(url, ":", port,
                   "/rest/mesures?token=", token,
                   "&codeSite=", codeSite,
                   "&codeVariable=", codeVariable,
                   "&codeDispositifDeCollecte=", codeDispositifDeCollecte,
                   "&dateFin=", dateF,
                   "&nombreMaxMesures=", maxNumberOfMeasurement,
                   sep="")
  }
  result <- jsonlite::fromJSON(txt=route)
  return (result)
}

# Tips & tricks:
# data[c("codeSite", "nomSite", "codeVariable", "nombreDeMesures")]
# data[1,]
# mean(data$resultat)
